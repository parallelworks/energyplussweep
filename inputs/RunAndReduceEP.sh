#!/bin/bash
# set -x

echo "Running EnergyPlus Sweep"

bin=$(cd $(dirname $0); pwd)

# split the params string and

while [ $# -gt 0 ]; do
  case $1 in
    --apps) apps=$2; shift 2;;
    --imf) imf=$2; shift 2;;
    --epw) epw=$2; shift 2;;
    --outxml) outxml=$2; shift 2;;
    --outdxf) outdxf=$2; shift 2;;
    --outpng) outpng=$2; shift 2;;
    --outcsv) outcsv=$2; shift 2;;
    --params) params=$2; shift 2;;
    *) echo $usage 1>&2
     exit 1;;
  esac
done

# unzip the apps
tar xzf $(pwd)/$apps

# add apps to PATH
appdir=${apps%.*}
export ENERGYPLUS_DIR=$(pwd)/$appdir/EnergyPlus-8-1-0
export NODE_DIR=$(pwd)/$appdir/node
export NODE_PATH=$(pwd)/$appdir/node/node_modules
export DXFPIPE_DIR=$(pwd)/$appdir/dxfpipeline
PATH=$ENERGYPLUS_DIR:$PATH
PATH=$NODE_DIR:$PATH
PATH=$NODE_PATH:$PATH
PATH=$DXFPIPE_DIR:$PATH

# read the parameters
IFS=',' read -a paramcut <<< "$params"
echo Parameter Length: "${#paramcut[@]}"

# CYCLE THROUGH THESE PARAMETERS AND SED THEM TO THE IMF FILE
for (( c=0; c<=${#paramcut[@]}; c=c+2 ))
do
	pname=${paramcut[$c]}
	pval=${paramcut[$c+1]}
	echo $pname
	echo $pval
	sed -i -e "/##set1 $pname\[\]/s/\[\].*/\[\] $pval/" $imf
done

runenergyplus $(pwd)/$imf $(pwd)/$epw
RC=$?

if [ $RC = 0 ]; then
  echo EnergyPlus application completed: RC=0
else
  echo EnergyPlus application failed: RC=$RC
fi

# generate plan image of the dxf result file
imfName=$(echo $(basename $imf) | cut -d '.' -f 1)
java -cp $(pwd)/$appdir/dxfpipeline/dxfpipeline.jar:$(pwd)/$appdir/dxfpipeline/lib/*:. org.kabeja.Loader -nogui -pipeline png $bin/Output/$imfName.dxf $outpng

# move the resulting xml file to the swift output
cp $bin/eplustbl.xml $outxml

# summary the xml results - this is still ugly parser needs improvement to output file direct to desired location
node $(pwd)/$appdir/parse.js $outxml
mv results/xml/*.csv $outcsv

# move the resulting dxf file to the swift output
cp $bin/Output/$imfName.dxf $outdxf

exit $RC

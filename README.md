# Swift Workflow for Parametric EnergyPlus Sweeps

This package details a Swift workflow for running parametric EnergyPlus models. 

* * *

A sample of the sweep file input is below:
```
pname                       pvals
ENDMONTH                    12
ENDDAY                      31
ORIENT                      0:180:45
W                           72
L                           72
FF                          3.85
NumberFloors                80,90,100
WWR                         70
```
* * *

Flags for the main.sh file are described below:

```
-sweep = parameter/variable input file
-execute = app executable at workers
-apps = directory of app binaries to send to workers
-epconfig = energyplus input file
-epweather = epw weather file
-count = true/false - if true only count the number of combinatorial simulations, if false run simulations
-sites = cloud/local - if local run on the local compute resource, if false run using persistant-coasters
-outdir = directory name for output files
```

* * *

**RUN INSTRUCTIONS**

On the headnode, clone the EnergyPlus workflow with the following command:

```
git clone git@bitbucket.org:parallelworks/energyplussweep.git
```

Add Swift to your path with the following command:

```
export PATH=$PATH:$PWD/apps/swift-0.96.0/bin
```

Start the headnode coaster service with the following command:

```
./coaster_tasks/startCoaster.sh
```

If running on Parallel.Works MesosRun pool, you should see EnergyPlus docker containers quickly connect to the coaster-service.

If running manually, on worker nodes pull the EnergyPlus docker container using the following command: 

```
docker pull mattshax/swift-0.96.0
```

On worker nodes, run the container and start the worker.pl coaster listener - ensure to replace the correct host address where specified:

```
docker run -d mattshax/swift-0.96.0 /bin/sh -c 'worker.pl <ENTER HOST ADDRESS HERE>:50100 dockerized /tmp/swiftwork'
```

Check the headnode coaster stats and ensure the worker nodes are connected. You should see something similar to below:

![Sample Coaster Stats](http://dev.parallel.works/working_files/energyplussweep/coaster_tasks/sampleStats.png)


Once workers are connected to the resource pool, on the headnode start the workflow with the following command:

```
./start.sh
```

or to run manually

```
./main.sh -sweep=inputs/test.sweep -execute=inputs/RunAndReduceEP.sh -apps=apps -epconfig=inputs/MAIN_RUN.imf -epweather=inputs/ep_weather -count=false -sites=cloud -outdir=results
```

Upon run completion, results are postprocessed and placed in desired outdir directory.

// BEAUTIFUL ENERGYPLUS CLUSTER AUTOMATION SCRIPT

var exec = require('child_process').exec;
var spawn = require('child_process').spawn;
var argv = require('minimist')(process.argv.slice(2));

// INPUT FLAGS HERE

    // mesos commands
    // i - instances - REQ
    // n - name
    // d - docker image
    // p - worker port
    // c - cpus
    // m - memory
    
    // swift commands
    // s - sweep file - REQ
    // e - ep file - REQ
    // w - weather file - REQ
    // o - outdir
    // t - count

// REQUIRED INPUTS

var reqFlag=0;
if (argv.hasOwnProperty("i")==true){var instances=argv.i;}else{console.log('-i Please define the number of instances you want to start.');reqFlag=1;}
if (argv.hasOwnProperty("s")==true){var sweep=argv.s}else{console.log('-s Please define your sweep file.');reqFlag=1;}
if (argv.hasOwnProperty("e")==true){var epconfig=argv.e;}else{console.log('-e Please define energyplus input file.');reqFlag=1;}
if (argv.hasOwnProperty("w")==true){var weather=argv.w;}else{console.log('-w Please define your weather file.');reqFlag=1;}
if (reqFlag==1){process.exit();}

console.log('\nStarting Parametric Energy Analysis...\n');

console.log('Number of Instances: '+instances);
console.log('Sweep File: '+sweep);
console.log('EnergyPlus File: '+epconfig);
console.log('Weather File: '+weather);

// OPTIONAL INPUTS

if (argv.hasOwnProperty("n")==true){var name=argv.n;}else{var name='swiftrun'+Math.floor(Math.random()*10000);}
if (argv.hasOwnProperty("d")==true){var dockerimage=argv.d;}else{var dockerimage='mattshax/precise_ep_node';}
if (argv.hasOwnProperty("p")==true){var port=argv.p;}else{function getRandomInt(min, max) {return Math.floor(Math.random() * (max - min + 1)) + min;}var port=getRandomInt(50100,50199);}
if (argv.hasOwnProperty("c")==true){var cpus=argv.c;}else{var cpus=0.3;}
if (argv.hasOwnProperty("m")==true){var memory=argv.m;}else{var memory=128;}
if (argv.hasOwnProperty("o")==true){var outdir=argv.o;}else{var outdir='results';}
if (argv.hasOwnProperty("t")==true){var count=argv.c;}else{var count=false;}

console.log('AppID: '+name);
console.log('Docker Container: '+dockerimage);
console.log('Worker Port: '+port);
console.log('CPU Size: '+cpus);
console.log('Memory Size: '+memory);
console.log('Output Directory: '+outdir);
console.log('Count Results Only: '+count);

var coasterService;
var pid;

function startCoasters(){

// there is a problem with starting coaster service via spawn

/*
console.log('\nStarting the Coaster Service on Worker Port: '+port);
var command = "/home/struct/swift-0.94.1/bin/coaster-service" 
var params = ["-p","50200","-localport",port,"-nosec","-passive"];
var coasterService = spawn(command,params);
var pid = coasterService.pid;
*/

var command = 'unset pids;coaster-service -p '+(port+100)+' -localport '+port+' -nosec -passive & pids="${pids-} $!"'

exec(command,function (error, stdout, stderr) {
    console.log(error);
    console.log(stderr);
    console.log(stdout);
});

 startCluster();
 
}

startCoasters();


// COASTER SERVICE REPORTING
// var result = '';
// coasterService.stdout.on('data', function(data) {
//      result += data.toString();
//     console.log(result)
// });

function startCluster(){
    console.log('\nSpawning the Docker Containers...');
    var command='curl -X POST -H "Content-Type: application/json" http://172.20.24.20:8080/v2/apps -d \'{"container": {"type": "DOCKER","docker": {"image": "'+dockerimage+'"}},"id": "'+name+'","instances": "'+instances+'","cpus": "'+cpus+'","mem": "'+memory+'","uris": [],"cmd": "/home/compute/worker.pl http://172.20.24.20:'+port+' dockerized /home/compute/swiftwork"}\'';
    exec(command, function (error, stdout, stderr) {
        checkSpawnStatus();
    })
}

//startCluster();

var spawnCheck;
function checkSpawnStatus(){
    var command='curl -X GET -H "Content-Type: application/json" http://172.20.24.20:8080/v2/apps/'+name;
    exec(command, function (error, stdout, stderr) {
        var status = JSON.parse(stdout).app;
        //console.log(status)
        console.log('  Containers: '+status.tasksRunning)
        if (status.tasksRunning==status.instances){
            console.log('Containers Successfully Spawned');
            console.log('\nStarting Energy Models...');
            setTimeout(function(){runEnergy();},5000);
        }else{
            setTimeout(function(){checkSpawnStatus()},3000);
        }
    })
}

function runEnergy(){


    var command = "./run.sh";
    var params = ["-sweep="+sweep,"-epconfig="+epconfig,"-epweather="+weather,"-count=false"];

    var energyRuns = spawn(command,params);
    
    var stdout='';
    energyRuns.stdout.on('data', function(data) {
        // stdout += data.toString();
         console.log(data.toString())
    });

    energyRuns.on('close', function (code) {
        console.log('Parametric Energy Analysis Complete');
        stopCluster();
    });


  
/*
    var command = "/home/struct/swift-0.94.1/bin/swift";
    var params = ["-config","cf","epsweep.swift","-sweep=test_run.sweep","-epconfig=GOAT.imf","-epweather=CHICAGO.epw ","-count=false"];

    var energyRuns = spawn(command,params);
    
    var stdout='';
    energyRuns.stdout.on('data', function(data) {
         stdout += data.toString();
         console.log(stdout)
    });

    energyRuns.on('close', function (code) {
        console.log('Parametric Energy Analysis Complete');
     //   stopCluster();
    });
*/


/*
    var command = "./run.sh -sweep=test.sweep -epconfig=GOAT.imf -epweather=CHICAGO.epw -count=false";

    exec(command, function (error, stdout, stderr) {
        
       console.log(stdout);
        
    });
*/
    
    
}

function stopCluster(){
    
    console.log('\nShuttting Down Docker Containers...');    
    var command='curl -X DELETE -H "Content-Type: application/json" http://172.20.24.20:8080/v2/apps/'+name;
    exec(command, function (error, stdout, stderr) {
        console.log('Containers Shut Down');
        var command = 'kill $pids'
        exec(command, function (error, stdout, stderr) {
            console.log('\nSimulation Complete');
            process.exit();
        });
    });

}

process.on('SIGINT', function(){
    console.log('\nKilling Rogue Processes...');    
    var command='curl -X DELETE -H "Content-Type: application/json" http://172.20.24.20:8080/v2/apps/'+name;
    exec(command, function (error, stdout, stderr) {
        var command = 'kill $pids'
        exec(command, function (error, stdout, stderr) {
        });
    });
});

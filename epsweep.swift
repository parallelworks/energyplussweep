
# ----- TYPE DEFINITIONS

  type file;
  
  type param {
    string pname;
    string pvals;
  }
  
  type pval {
    string name;
    string[] values;
  }


# ----- APP DEFINITIONS

  app (file xml,file dxf,file png,file csv) runEP ( file apps, file execute, file imf, file epw, string params )
  {
    bash @execute "--apps" @apps "--imf" @imf "--epw" @epw "--outxml" @xml "--outdxf" @dxf "--outpng" @png "--outcsv" @csv "--params" params;
  }


# ----- EXTERNAL INPUTS

  string appsFile=@strcat(@arg("apps","apps"),".tgz");
  file apps <single_file_mapper; file=appsFile>;
  
  file execute <single_file_mapper; file=@arg("execute","inputs/RunAndReduceEP.sh")>;
  
  file epconfig  <single_file_mapper; file=@arg("epconfig",  "inputs/MAIN_RUN.imf")>;
  
  string epweather=@arg("epweather", "inputs/ep_weather");
  file weatherFiles[] <filesys_mapper;location=epweather,suffix=".epw">;
  
  param  pset[] = readData(@arg("sweep","inputs/test.sweep"));
  
  string outdir=@arg("outdir","results");
  string count=@arg("count","true");


# ----- INTERNAL FUNCTIONS

  (string[] r) extend(string[] a, string v,string n, int lastIndex) {
    foreach ov, i in a {r[i] = a[i];}
    string[] f;f[0]=n;f[1]=v;
    r[lastIndex]=@strjoin(f,",");
  }
  
  iterateEP(file a, file e, file c, file w, string[] parameters, string d) {
    string ap=@strjoin(parameters,",");
    string[] apx=@strsplit(ap,",");
    string[] aval;
    foreach v, vn in parameters {
      string[] an=@strsplit(v,",");
      aval[vn]=an[1];
    }
    string fileid = @strcat("ep+",@strjoin(aval,"+"));
    tracef("%s\n",fileid);
    file outxml  <single_file_mapper; file=@strcat(d,"/xml/",fileid,".xml")>;  
    file outdxf  <single_file_mapper; file=@strcat(d,"/dxf/",fileid,".dxf")>;  
    file outpng  <single_file_mapper; file=@strcat(d,"/png/",fileid,".png")>;  
    file outcsv  <single_file_mapper; file=@strcat(d,"/csv/",fileid,".csv")>;  
    (outxml,outdxf,outpng,outcsv) = runEP (a,e,c,w,ap);
  }

  dynamicFor(pval[] vars, string[] values, int index, int len, file ap, file exc, file epc, file epw, string od) {
    if (index < len) {
      foreach v in vars[index].values {dynamicFor(vars, extend(values, v,vars[index].name, index), index + 1, len, ap, exc, epc, epw, od);}
    }else {iterateEP(ap, exc,epc,epw,values,od);}
  }

  (int sz) countAllRecursive(pval[] pval, int index) {
    if (index < 0) {sz = 1;}
    else {sz = @length(pval[index].values) * countAllRecursive(pval, index - 1);}
  }

  (int sz) countAll(pval[] pval) {
    if (@length(pval) == 0) {sz = 0;}
    else {sz = countAllRecursive(pval, @length(pval) - 1);}
  }


# ----- PARAMETER SETUP & LAUNCH
  
  string[] empty;
  pval[] pvals;

  foreach p, pn in pset {
      string val[]=@strsplit(p.pvals,",");
      pvals[pn].name=p.pname;
      foreach v, vn in val {
        pvals[pn].values[vn]=v;
      }
  }
  
  int runs=countAll(pvals);
  tracef("\n%i Runs in Simulation\n\n",runs);
  
  if (count=="false"){
      dynamicFor(pvals, empty, 0, @length(pvals), apps, execute, epconfig, weatherFiles[0], outdir);
  }

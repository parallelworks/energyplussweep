#! /bin/sh
# set -x

export SWIFT_HEAP_MAX=4G
dir=$(cd $(dirname $0); /bin/pwd)
export SWIFT_USERHOME=$dir/swifthome

# extract the run parameters
usage="$0 -sweep=inputs/test.sweep -execute=inputs/RunAndReduceEP.sh -apps=apps -epconfig=inputs/MAIN_RUN.imf -epweather=inputs/ep_weather -count=true -sites=local -outdir=results"
SWEEP=$*;

echo ""; echo Running EnergyPlus Swift Workflow;echo "";

while [ $# -gt 0 ]; do
  case $1  in
    *-sweep*)  sweep=$(echo $1 | sed 's/-sweep=/\n/g'); echo Sweep: $sweep; shift ;;
    *-execute*)  execute=$(echo $1 | sed 's/-execute=/\n/g'); echo Execute: $execute; shift ;;
    *-apps*)  apps=$(echo $1 | sed 's/-apps=/\n/g'); echo Apps: $apps; shift ;;
    *-epconfig*)  epconfig=$(echo $1 | sed 's/-epconfig=/\n/g'); echo EPconfig: $epconfig; shift ;;
    *-epweather*)  epweather=$(echo $1 | sed 's/-epweather=/\n/g'); echo EPweather: $epweather; shift ;;
    *-count*)  count=$(echo $1 | sed 's/-count=/\n/g'); echo Count: $count; shift ;;
    *-sites*)  sites=$(echo $1 | sed 's/-sites=/\n/g'); echo Sites: $sites; shift ;;
    *-outdir*)  outdir=$(echo $1 | sed 's/-outdir=/\n/g'); echo Outdir: $outdir; shift ;;
    *) echo "no match"; shift ;;
  esac
done

# add swift and node to the path
export PATH=$dir/apps/swift-0.96.0/bin:$dir/apps/EnergyPlus-8-1-0:$dir/apps/node:$dir/apps/node/node_modules:$PATH

# generate the expanded sweep file
node utils/presweep.js $sweep;SWEEPNEW=$(echo $SWEEP | sed 's/.sweep/_run.sweep/2')

# tar the app and openfoam input file directories
echo Compressing Applications;
tar czf $apps.tgz $apps;
echo Compression Complete;echo "";

# run the energyplus sweep via swift
swift -ui http:3018 -sites $sites -configpath swift.config epsweep.swift $SWEEPNEW

for f in $outdir/csv/*.csv; do 
    cat $f >> $outdir/results.csv
    echo "" >> $outdir/results.csv
done

# move the vtk files to the paraview data_library
#DATE=$(date +%s)
#mv results/vtk /core/working_files/openfoam/data_library/openfoam_$DATE

#./utils/clean.sh
